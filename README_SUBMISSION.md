DST using Maximum Entropy Markov Model

Notes:
* Using words in the sentences in each turn to predict the slots
* Slots predicted jointly
* Using the information of slots from the previous turn (Markov chain)

Requirements:
* python3
* nltk

Usage:
1) To train: python tracker.py --train --model <model_name>
2) To evaluate on dev set: python tracker.py --eval_dev --model <model_name>
3) To evaluate on test set: python tracker.py --eval_test --model <model_name>

You can use those three flags --train, --eval_<dev/test> at once

Experiments:
1) model1.pkl
* Using unigram and bigram features
* MEMM parameter: feature cutoff=5, iteration=25
* Result on dev set: 

				food_type	|     area	|  price_range	|  overall
		----------------------------------------------------------------------------
		Accuracy(%) 	19.40		|    25.13	|    31.93	|  28.21 
		F1 score      0.074		|    0.137	|    0.319	|  0.177 


* Result on test set:

				food_type	|     area	|  price_range	|  overall
		----------------------------------------------------------------------------
		Accuracy(%) 	21.40		|    29.77	|    39.54	|  30.24 
		F1 score      0.082		|    0.183	|    0.306	|  0.191 


