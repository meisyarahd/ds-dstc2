#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Stub to fill in ML model
"""
import argparse
import sys
import numpy as np
# import tensorflow as tf
import json
from collections import Counter, defaultdict
from string import punctuation
from nltk.classify import maxent
import nltk
import pickle

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score, f1_score

from tracker.dataset.dstc2 import Dstc2


# Data load
def preprocess_label(label):
    """Convert string formated labels into tuples"""
    lab = label.split()
    if len(lab) == 3:
        return tuple(lab)
    else:
        return tuple((lab[0]+" "+lab[1], lab[2], lab[3]))

def load_data(filename):
    """
    Extract input and output for the model.
    Only use the ASR output as the input data
    """
    with open(filename) as f:
        data = json.load(f)
    input_data = []
    for dial in data:
        turn_seq = []
        for turn in dial:
            text = turn[0] + " | " + turn[1]
            label = preprocess_label(turn[4])
            turn_seq.append((text, label))
        input_data.append(turn_seq)
    return input_data

# Feature extaction
def vectorizer(sentences):
    bigram_vec = CountVectorizer(ngram_range=(1, 2), token_pattern=r'\b\w+\b', min_df=1)
    bigram_vec.fit(sentences)
    return vectorizer

def get_prev_label(i, dialog):
    if i == 0:
        prev_label = ('none', 'none', 'none')
    else:
        prev_label = dialog[i-1][1]


def extract_features(turn, history):
    """
    Features for MEMM
    """
    # training
    sent = [w.lower() for w in turn[0] if w not in punctuation]

    unigram_features = {k:1 for k in set(sent)}
    bigrams = list(zip(sent[:-1], sent[1:]))
    bigram_features = {k:1 for k in set(bigrams)}
    prev_label = history[-1]

    features = {}
    features["label_food"] = prev_label[0]
    features["label_area"] = prev_label[1]
    features["label_price"] = prev_label[2]
    features.update(unigram_features)
    # features.update(bigram_features)
    return features


def training(raw_train_data):
    """
    Train MEMM using train set. Return the model
    """
    train_set = []
    for dialog in raw_train_data:
        unlabeled_dialog = nltk.tag.untag(dialog)
        # print (dialog)
        history = [('none', 'none', 'none')]
        for i, (turn, label) in enumerate(dialog):
            # print (i, turn)
            features = extract_features(turn, history)
            train_set.append( (features, label) )
            history.append(label)
    start_time = time.time()
    print ("Train starts at", start_time)
    model = maxent.MaxentClassifier.train(train_set,
                                               algorithm='IIS',
                                               count_cutoff=10, max_iter=10)
    duration = time.time() - start_time
    print ("Training time:", duration)
    return model #, duration

def decode(dialog, model):
    """
    Return a sequence of predicted slots of a dialog
    """
    # with open(, "rb") as f:
    #     model = pickle.load(f)
    pred_labels = [('none', 'none', 'none')]
    for i, turn in enumerate(dialog):
        features = extract_features(turn, pred_labels)
        label = model.classify(features) ### IMPORTANT
        pred_labels.append(label)
    # return zip(dialog, pred_labels[1:])
    return pred_labels[1:]

def testing(raw_test_data, model):
    """
    Predict slots on unlabeled dialogs
    """
    labels = []
    for unlabeled_dialog in raw_test_data:
        label = decode(unlabeled_dialog, model)
        labels.append(label)
    return labels

def compute_accuracy_f1(true_labels, pred_labels):
    n = len(true_labels[0])
    accs, f1s = [],[]
    for i in range(n):
        true = [x[i] for x in true_labels]
        pred = [x[i] for x in pred_labels]
        acc = accuracy_score(true, pred)
        accs.append(acc)
        f1 = f1_score(true, pred, average="weighted")
        f1s.append(f1)
        print("accuracy label ",i, ": ", acc)
        print("F1 score label ",i, ": ", f1)
    overall_acc =  np.mean(accs)
    overall_f1 = np.mean(f1s)
    return overall_acc, overall_f1

def evaluate(model, eval_set):
    # EVAL_SET = valid_set
    print ("Validating")
    unlabeled_test_dialogs = []
    for test_dialog in eval_set:
        s = nltk.tag.untag(test_dialog)
        unlabeled_test_dialogs.append(s)
    dialog_labels = testing(unlabeled_test_dialogs, model)

    print ("Evaluation")
    true_labels = [x[1] for d in eval_set for x in d]
    pred_labels = [x for d in dialog_labels for x in d]
    accuracy, f1 = compute_accuracy_f1(true_labels, pred_labels)
    print ("Overall accuracy: ", accuracy)
    print ("Overall F1 score: ", f1)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--train", default=False, action="store_true", help="Train mode")
    parser.add_argument("--eval_dev", default=False, action="store_true", help="Evaluation on dev set")
    parser.add_argument("--eval_test", default=False, action="store_true", help="Evaluation on test set")
    parser.add_argument("--model", default="model.pkl", type=str, help="Model name saved during training, or used for evaluation")
    args = parser.parse_args()

    if args.train:
        train_set = load_data('data/dstc2/data.dstc2.train.json')
        print ("Training")
        model = training(train_set)
        pickle.dump(model, open(args.model, "wb"))
    if args.eval_dev:
        eval_set = load_data('data/dstc2/data.dstc2.dev.json')
        with open(args.model, "rb") as f:
            model = pickle.load(f)
        evaluate(model, eval_set)
    if args.eval_test:
        eval_set = load_data('data/dstc2/data.dstc2.test.json')
        with open(args.model, "rb") as f:
            model = pickle.load(f)
        evaluate(model, eval_set)
